import dockerSchema from '../schema/schema.json'
var Ajv = require('ajv')
var ajv = new Ajv({
  meta: false,                  // optional, to prevent adding draft-06 meta-schema
  extendRefs: true,             // optional, current default is to 'fail', spec behaviour is to 'ignore'
  unknownFormats: 'ignore',     // optional, current default is true (fail)
  schemaId: 'id',
  coerceTypes: true
})

const metaSchema = require('ajv/lib/refs/json-schema-draft-04.json')
ajv.addMetaSchema(metaSchema)
ajv._opts.defaultMeta = metaSchema.id

ajv._refs['http://json-schema.org/schema'] =
  'http://json-schema.org/draft-04/schema'

ajv.addFormat("duration", /^P(\d+Y)?(\d+M)?(\d+W)?(\d+D)?(T(\d+H)?(\d+M)?(\d+S)?)?$/)
const validate = ajv.compile(dockerSchema[3]) 

export default function validator (testSchema) {
  if (validate(testSchema)) {
    console.log('Yml is valid')
    return null
  } else {
    console.log('Data is INVALID!')
    console.log(validate.errors)
    return validate.errors
  }    
}