import React, { Component } from 'react'
import ReactJson from 'react-json-view'
import TestSchema from '../schema/testSchema.json'
import ymlValidator from './validation' 
import '../styles/JsonTree.css'
import TransitionAlerts from './core/alert'

export class JsonTree extends Component {
  constructor (props) {
    super(props)
    this.state = {
      src: TestSchema[0],
      container_name: "db",
      project_name: "ohl",
      containers: [],
      message: [],
      severity: "",
      isValid: false
    }
  }
  static defaultProps = {
    src: null,
    onAdd: true,
    onEdit: true,
    onDelete: true
  }

  updateProjectName = (e) => {
    this.setState({project_name: e.target.value})
  }
  updateContainerName = (e) => {
    this.setState({container_name: e.target.value});
  }
  validateYml () {
    const errors = ymlValidator(this.state.src);
    this.setState({ isValid: true});
    if(errors === null){
      this.setState({message: ['Yaml is valid'], severity: 'success'})
    }
    else{
      this.setState({message: errors, severity: 'error'})
    }
  }

   onAdd = async e => {
    console.log(e)
    await this.setState({ src: e.updated_src })
    this.validateYml()
  }

  onEdit = async e => {
    await this.setState({ src: e.updated_src })
    this.validateYml()
  }

  onDelete = async e => {
    console.log(e)
    await this.setState({ src: e.updated_src })
    this.validateYml()
  }

  render () {
    const { onAdd, onEdit, onDelete } = this.props
    const { src } = this.state
    if(this.state.src.services){
      this.state.containers = Object.keys(this.state.src.services)
    }
    return (
      <div>
         {this.state.isValid && <TransitionAlerts message={this.state.message} severity={this.state.severity} project={this.state.project_name}/> }
        <div className="JsonTree">
          <p>Project Name: <input className="text-field" type="text" value={this.state.project_name} onChange={this.updateProjectName} />
        </p>
          <p>Primary Container: <select className="text-field" onChange={this.updateContainerName}>
            {this.state.containers.map((container,index) => <option key={index}>{container}</option>)}</select></p>
        </div>
        <div className='JsonTree'>
          <ReactJson
            name={this.state.project_name}
            iconStyle='circle'
            src={src}
            onEdit={onEdit ? this.onEdit : false}
            onDelete={onDelete ? this.onDelete : false}
            onAdd={onAdd ? this.onAdd : false}
            displayDataTypes={false}
            displayObjectSize={false}
          />
        </div>
          {/* <div className='wrapper'>
            <button onClick={() => this.validateYml()}> Validate </button>
          </div> */}
      </div>
    )
  }
}

export default JsonTree
