import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function TransitionAlerts(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const messageArray = props.message
  const severity = props.severity? props.severity : "info"

  const formatMessage = (message) => {
    if(severity === 'error') {
      if(message.dataPath !== "")
        if(message.keyword !== "additionalProperties")
          return `${message.keyword} in ${message.dataPath} ${message.message}`
        else
          return ` ${message.dataPath} ${message.message}`
      else 
        return `${props.project} ${message.message}`
    }
    else{
      return message
    }
  }

  return (
    <div className={classes.root}>
      <Collapse in={open} >
      {messageArray.map((message, index) => 
      <Alert key={index}
      severity={severity}
        action={
          <IconButton
            aria-label="close"
            color="inherit"
            size="small"
            onClick={() => {
              setOpen(false);
            }}
          >
          </IconButton>
        }
      >
        {formatMessage(message)}
      </Alert>
      )}
      </Collapse>
    </div>
  );
}
