import React from 'react';
import './App.css';
import JsonTree from './components/JsonTree'
function App() {
  return (
    <JsonTree />
  );
}

export default App;
